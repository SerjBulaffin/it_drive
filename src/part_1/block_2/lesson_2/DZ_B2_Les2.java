package part_1.block_2.lesson_2;

import java.util.Scanner;

public class DZ_B2_Les2 {

    public static double f(double x) {
        return x * x;
    }

    //Расчет интеграла по формуле квадрата
    public static double integral(double a, double b, int n) {
        double h = (b - a) / n;
        double result = 0;
        int count = 0;

        for (double x = a; x <= b; x += h) {
            double currentRecnangle = f(x) * h;
            result += currentRecnangle;
            count++;
        }
        printTable("Integral Rectangle", count, result);
        return result;
    }

    //Расчет интеграла по формуле Симпсона
    public static double integralSimpsona(double a, double b, int n) {
        double result = 0;
        double h = (b - a) / n;
        int count = 0;

        for (double x = a; x <= b; x += h * 2) {
            double currentResult = f(x - h) + 4 * f(x) + f(x + h);
            result += currentResult;
            count++;
        }

        printTable("Integral Simpson   ", count, ((h / 3) * result));
        return (h / 3) * result;
    }

    public static void printTable(String integralName, int count, double result) {
        System.out.printf("| %-20s | %7d |  %-20s |\n", integralName, count, result);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        int n = scanner.nextInt();

        double result = integral(a, b, n);
        double resultSimpson = integralSimpsona(a, b, n);
    }
}
