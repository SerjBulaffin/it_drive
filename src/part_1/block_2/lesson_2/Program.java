package part_1.block_2.lesson_2;

import java.util.Scanner;

public class Program {

    public static double f(double x) {
        return x * x;
    }

    public static double integral (double a, double b, int n) {
        double h = (b - a) / n;

        double result = 0;

        for (double x = a; x <= b; x += h) {
            double currentRectangle = f(x) * h;
            result += currentRectangle;
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        int n = scanner.nextInt();

        double result = integral(a, b, n);

        System.out.println(result);
    }
}
