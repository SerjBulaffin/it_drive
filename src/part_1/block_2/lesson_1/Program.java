package part_1.block_2.lesson_1;

public class Program {
    public static void main(String[] args) {
        int[] array = {0, -2, -10, 12, 15, 19, 23};

        System.out.println(minArray(array));
        System.out.println(average(array));
    }

    public static int minArray(int[] arr) {
        int min = arr[0];
        for (int i = 1; i < arr.length; i++)
            if (min > arr[i]) {
                min = arr[i];
            }
        return min;
    }

    public static double average(int[] arr) {
        double summ = 0;
        for (int i = 0; i < arr.length; i++) {
            summ += arr[i];
        }
        return summ / arr.length;
    }
}
