package part_1.block_2.lesson_1;

import java.util.Scanner;

public class DZ_B2_Les1_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int digit = scanner.nextInt();
        System.out.println(digitReverse(digit));
    }

    public static int digitReverse(int a) {
        int reverse = 0;
        int step = 10;
        while (a != 0) {
            int tmp = a % 10;
            reverse = (reverse * step) + tmp;
            a /= 10;
            //step = 10;
        }
        return reverse;
    }
}

//2. Написать функцию, возвращающую зеркальное представление числа.
