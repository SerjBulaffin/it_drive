package part_1.block_2.lesson_1;

import java.util.Scanner;

public class DZ_B2_Les1_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int digit = scanner.nextInt();
        System.out.println(summForDigit(digit));
    }

    public static int summForDigit(int a) {
        int summ = 0;
        while (a != 0) {
            summ += a % 10;
            a /= 10;
        }
        return summ;
    }
}

//1. Написать функцию, возвращающую сумму цифр определенного числа.
