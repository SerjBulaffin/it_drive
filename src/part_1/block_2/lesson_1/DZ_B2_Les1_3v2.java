package part_1.block_2.lesson_1;

public class DZ_B2_Les1_3v2 {
    public static void main(String[] args) {
        int[] array = {1, 56, 87, 24, 15, 16, 98, 71, 43, 46, 97, 89};
        System.out.println(summOfDiap(array, 2, 5));
    }

    public static int summOfDiap (int[] arr, int a, int b) {
        int summ = 0;
        for (int i = a; i <= b; i++) {
            summ += arr[i];
        }
        return summ;
    }
}
