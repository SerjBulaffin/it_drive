package part_1.block_2.lesson_1;

import java.util.Scanner;

public class DZ_B2_Les1_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int digit = scanner.nextInt();
        System.out.println(summOfDiap(digit, 2, 5));
    }

    public static int summOfDiap(int dig, int a, int b) {
        int digR = digitReverse(dig);
        int summ = 0;
        for (int i = 1; i <= b; i++) {
            int tmp = digR % 10;
            if (i >= a)
                summ += tmp;
            digR /= 10;
        }
        return summ;
    }

    public static int digitReverse(int a) {
        int iter = 1;
        int aForIter = a;
        while (aForIter > 0) {
            aForIter /= 10;
            iter *= 10;
        }
        iter /= 10;

        int reverse = 0;
        for (int i = iter; i > 0; i /= 10) {
            reverse += a % 10 * i;
            a /= 10;
        }
        return reverse;
    }
}
//3. Написать функцию, которая возвращает сумму чисел, которые находятся в диапазоне от a до b.