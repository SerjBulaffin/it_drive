package part_1.block_2.lesson_3;

import java.util.Scanner;

public class DZ_B2_Les3_3 {

    //возведение в степень
    public static double pow(double x, int y) {
        double result = x;
        for (int i = 1; i < y; i++) {
            result *= x;
        }
        return result;
    }

    //расчет интеграла методом прямоугольника
    public static double integral(double a, double b, double n, int y) {
        double result = 0;
        double h = (b - a) / n;
        for (double x = a; x <= b; x += h) {
            result += pow(x, y) * h;
        }
        return result;
    }

    //расчет интеграла методом Симпсона
    public static double integralSimpson(double a, double b, double n, int y) {
        double result = 0;
        double h = (b - a) / n;
        for (double x = a; x <= b; x += h * 2) {
            result += pow((x - h), y) + 4 * pow(x, y) + pow((x + h), y);
        }
        return (h / 3) * result;
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter - a -> ");
        double a = scanner.nextDouble();
        System.out.print("Enter - b -> ");
        double b = scanner.nextDouble();

        int ns[] = {100, 1000, 10000, 100000, 1000000}; // разбиения
        int ys[] = {2, 3, 4, 5, 6, 7}; // степени

        for (int i = 0; i < ns.length; i++) {
            for (int j = 0; j < ys.length; j++) {
                double result = integralSimpson(a, b, ns[i], ys[j]);
                System.out.println("Simpson - " + ns[i] + " - x^" + ys[j] + " = " + result);
            }
        }

        System.out.println();

        for (int i = 0; i < ns.length; i++) {
            for (int j = 0; j < ys.length; j++) {
                double result = integral(a, b, ns[i], ys[j]);
                System.out.println("Rectangle - " + ns[i] + " - x^" + ys[j] + " = " + result);
            }
        }
    }
}
/*
3) Реализовать программу, которая содержит:
- написанную Вами функцию pow(double x, int y) - она возводит число x в произвольную степень y.
- две функции для расчета интеграла методом Симпсона и методом прямоугольников. Функции должны иметь вид:
double integral(double a, double b, double n, int y)
Таким образом, данные функции выполняют вычисления интеграла функции f(x^y).
В main описать два массива:
int ns[] = {100, 1000, 10000, 100000, 1000000}; // разбиения
int ys[] = {2, 3, 4, 5, 6, 7}; // степени
Обе функции для вычисления интеграла следует "прогнать" по указанным массивам.
Результаты вычисления обеих функций для ns и ys следует вывести на экран.
Например:
Simpson - 100 - x^2 = значение
Simpson - 1000 - x^2 = значение
...
Simpson - 1000000 - x^7 = значение
и т.д.
 */
