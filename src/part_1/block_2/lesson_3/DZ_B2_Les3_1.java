package part_1.block_2.lesson_3;

public class DZ_B2_Les3_1 {

    public static void arraySort(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            count++;
            for (int j = i + 1; j < arr.length; j++) {
                count++;
                if (arr[i] > arr[j]) {
                    int tmp = arr[j];
                    arr[j] = arr[i];
                    arr[i] = tmp;
                }
            }
        }
        System.out.println("Количество итераций сортировка обычная - " + count);
    }

    public static void arraySortVibor(int[] arr) {
        int min, indexOfMin;
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            count++;
            min = arr[i];
            indexOfMin = i;
            for (int j = i + 1; j < arr.length; j++) {
                count++;
                if (arr[j] < min) {
                    min = arr[j];
                    indexOfMin = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[indexOfMin];
            arr[indexOfMin] = temp;
        }
        System.out.println("Количество итераций сортировка выбором - " + count);
    }

    public static void main(String[] args) {
        int[] array = {25, 45, 98, 71, -12, -2, 68, 31, 99, -17};
        int[] array1 = {25, 45, 98, 71, -12, -2, 68, 31, 99, -17};

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        System.out.println();

        arraySort(array);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

        System.out.println();

        //Сортировка выбором
        for (int i = 0; i < array1.length; i++) {
            System.out.print(array1[i] + " ");
        }

        System.out.println();

        arraySortVibor(array1);
        for (int i = 0; i < array1.length; i++) {
            System.out.print(array1[i] + " ");
        }

    }
}
//1) Реализовать процедуру сортировки выбором.
