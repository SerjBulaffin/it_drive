package part_1.block_2.lesson_3;

public class DZ_B2_Les3_2 {

    //функция бинарного поиска, возвращаемый тип boolean
    public static boolean binarySearch(int[] arr, int search) {
        int left = 0;
        int right = arr.length - 1;
        int middle;
        boolean find = false;
        while (left <= right) {
            middle = (right + left) / 2;
            if (arr[middle] < search) {
                left = middle + 1;
            } else if (arr[middle] > search) {
                right = middle - 1;
            } else {
                find = true;
                break;
            }
        }
        return find;
    }

    //сортировка выбором
    public static void arraySortVibor(int[] arr) {
        int min, indexOfMin;
        for (int i = 0; i < arr.length; i++) {
            min = arr[i];
            indexOfMin = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < min) {
                    min = arr[j];
                    indexOfMin = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[indexOfMin];
            arr[indexOfMin] = temp;
        }
    }

    public static void main(String[] args) {
        int[] array = {12, 45, 78, 65, 78, 12, 34, 48, 611, 12354, -125, -7, -3};

        int numberForSearch = 1235;

        boolean findNumber = binarySearch(array, numberForSearch);
        System.out.println(findNumber);
    }
}

//2) Реализовать функцию бинарного поиска через цикл.
