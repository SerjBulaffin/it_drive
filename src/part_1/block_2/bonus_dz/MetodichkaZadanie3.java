package part_1.block_2.bonus_dz;

import java.util.Scanner;

public class MetodichkaZadanie3 {

    public static double pow(double x, int y) {
        double result = x;
        for (int i = 1; i < y; i++) {
            result *= x;
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double num = scanner.nextDouble();
        int step = scanner.nextInt();

        while (num != -1) {
            System.out.println(pow(num, step));
            num = scanner.nextDouble();
            step = scanner.nextInt();
        }
    }
}
