package part_1.block_2.bonus_dz;

public class MetodichkaZadanie4 {

    public static void sort(char[] arr) {
        char min;
        int minIndex;

        for(int i = 0; i < arr.length; i++) {
            min = arr[i];
            minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < min) {
                    min = arr[j];
                    minIndex = j;
                }
            }
            char temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }
    }

    public static void sortTwoArrays(char[] arr1, int[] arr2) {
        int min;
        int minIndex;

        for(int i = 0; i < arr2.length; i++) {
            min = arr2[i];
            minIndex = i;
            for (int j = i + 1; j < arr2.length; j++) {
                if (arr2[j] > min) {
                    min = arr2[j];
                    minIndex = j;
                }
            }
            int temp = arr2[i];
            char temp2 = arr1[i];
            arr2[i] = arr2[minIndex];
            arr1[i] = arr1[minIndex];
            arr2[minIndex] = temp;
            arr1[minIndex] = temp2;
        }
    }

    public static void main(String[] args) {
        //Scanner scanner = new Scanner(System.in);
        //String str = scanner.nextLine();
        String str = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASSSSSSSSSSSSSSSSSSSSSSSSDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDWEWWKFKKDKKDSKAKLSLDKSKALLLLLLLLLLRTRTETWTWWWWWWWWWWOOOOOOO";
        char[] array = str.toCharArray();
        sort(array);

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
        }
        System.out.println();

        char[] simbols = new char[10];
        int[] counting = new int[10];

        int count = 1;
        int indexOfArray = 0;
        char ch = array[0];

        for (int i = 1; i < array.length; i++) {
            if (ch == array[i]) {
                count++;
                if (count > 999)
                    return;
            } else {
                simbols[indexOfArray] = ch;
                counting[indexOfArray] = count;
                ch = array[i];
                indexOfArray++;
                count = 1;
                if (indexOfArray > 9)
                    break;
            }
        }

        for (int i = 0; i < simbols.length; i++) {
            System.out.print(simbols[i] + " = " + counting[i] + ", ");
        }

        System.out.println();

        sortTwoArrays(simbols, counting);
        for (int i = 0; i < simbols.length; i++) {
            System.out.print(simbols[i] + " = " + counting[i] + ", ");
        }

        System.out.println();

        //доделать вывод на печать
        /*for (int i = 0; i < 10; i++) {
            System.out.print(counting[i]);
            for (int j = 0; j < 10; j++) {
                System.out.print("#");
                if (counting[i] * 10 / counting[0] < i)
                    continue;
                if (j < simbols.length - 1)
                    System.out.print(simbols[j] + " ");
            }
            System.out.println();
        }*/

        for (int i = 0; i < 11; i++) {
            int tmp = counting[i] * 10 / counting[0];

            for (int j = 0; j <= i; j++) {
                if (tmp <= i) {
                    System.out.print(counting[i]);
                    continue;
                }
                System.out.print("# ");
            }
            System.out.println();
        }
    }
}
