package part_1.block_2.lesson_6;

import java.util.Scanner;

/*
2) Реализовать функцию, которая принимает на вход массив символов-цифр, например: {'2','3','1','1'},
а возвращает тип int -> 2311. (Преобразование строки в число). Строка - массив символов.
 */
public class DZ_Block2_Lesson6_Zadanie2 {

    //проверка char является ли он числом, если да возвращаем true
    public static boolean isDigit(char character) {
        return character >= '0' && character <= '9';
    }

    //переводит символ в int число (если при вызове isDigit() true), путем вычитания из кода символа char
    //кода символа '0'
    public static int toInt(char digit) {
        if (isDigit(digit)) {
            return digit - '0';
        }
        return -1;
    }

    //Собираем из массива char челочисленное число
    public static int massCharToInt(char[] array) {
        int result = 0;
        int razryad = 1;

        for (int i = array.length - 1; i >= 0; i--) {
            int tmp = toInt(array[i]);  //проверяем является ли символ числом
            if (tmp != -1) {    //если в tmp вернулся не -1, кладем в результат и умножаем на степенной результат
                result += tmp * razryad;
                razryad *= 10;
            } else {
                return result = -1;    // иначе кладем в результат -1, прерываем цикл
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char[] digits = scanner.nextLine().toCharArray();
        int result = massCharToInt(digits);
        System.out.println(result);
    }
}
