package part_1.block_2.lesson_6;

import java.util.Scanner;

public class Program {

    public static boolean isUpper(char letter) {
        return letter >= 'A' && letter <= 'Z';
    }

    public static boolean isDigit(char character) {
        return character >= '0' && character <= '9';
    }

    public static int toInt(char digit) {
        if (isDigit(digit)) {
            return digit - '0';
        }
        return -1;
    }

    public static int stringsCompare(char[] a, char[] b) {
        return -1;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char[] text = scanner.nextLine().toCharArray();
    }
}
