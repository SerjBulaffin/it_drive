package part_1.block_2.lesson_6;

/*
3) Реализовать функцию, которая выводит наиболее часто встречающиеся буквы в тексте (первую тройку),
если количество букв совпадает - вывести первую тройку в алфавитном порядке.
 */
public class DZ_Block2_Lesson6_Zadanie3 {

    //сортировка двух синхронизованных массивов по значению int
    public static void sortTwoArrays(char[] arr1, int[] arr2) {
        int min;
        int minIndex;

        for(int i = 0; i < arr2.length; i++) {
            min = arr2[i];
            minIndex = i;
            for (int j = i + 1; j < arr2.length; j++) {
                if (arr2[j] > min) {
                    min = arr2[j];
                    minIndex = j;
                }
            }
            int temp = arr2[i];
            char temp2 = arr1[i];
            arr2[i] = arr2[minIndex];
            arr1[i] = arr1[minIndex];
            arr2[minIndex] = temp;
            arr1[minIndex] = temp2;
        }
    }

    public static String threeCharMaxCountToString(char[] array) {
        //создаем массив на 123 элемента, дальше идут скобки и херь
        int[] search = new int[123];

        //подсчет букв,нумерация массива идет по char кодам символов, если находим символ, увеличиваем значение на +1
        //int max1 = 0;
        for (int i = 0; i < array.length; i++) {
            search[array[i]] += 1;
        }

        //создание двух синхронизованных массивов
        char[] charArray = new char[58];
        int[] countArray = new int[58];

        //заполнение двух массивов из входного array
        int searchCount = 0;
        for (int i = 65; i < search.length; i++) {
            if (search[i] > 0) {
                charArray[searchCount] = (char) i; //массив символов
                countArray[searchCount] = search[i]; // массив значений
                searchCount++;
            }
        }

        //сортировка двух массивов по количеству букв по убыванию
        sortTwoArrays(charArray, countArray);

        //вывод 3-х первых элементов по убыванию, если количество совпадает, то выводится по алфавиту
        String str = "";
        for (int i = 0; i < 3; i++) {
            str += charArray[i] + " " + countArray[i] + "\n";
        }
        return str;
    }


    public static void main(String[] args) {
        //Scanner scanner = new Scanner(System.in);
        //char[] array = scanner.nextLine().toCharArray();
        String str = "erttyyghdvvcxbbbbbbdfrrtkjhljoihftfdgaaaaafjhgjmnnngfcccccfssasfeeeehhjh"; //default array
        /*
        a 8
        f 7
        h 7
         */
        char[] array = str.toCharArray();
        System.out.println("Старый вывод");
        String strOut = threeCharMaxCountToString(array);
        System.out.println(strOut);

    }
}
