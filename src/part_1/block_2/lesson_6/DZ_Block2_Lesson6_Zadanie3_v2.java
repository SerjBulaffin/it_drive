package part_1.block_2.lesson_6;

/*
3) Реализовать функцию, которая выводит наиболее часто встречающиеся буквы в тексте (первую тройку),
если количество букв совпадает - вывести первую тройку в алфавитном порядке.
 */
public class DZ_Block2_Lesson6_Zadanie3_v2 {

    //Сортировка двухмерного массивов
    public static void intsCompare(int[][] array) {
        int min, indexOfMin;

        for (int i = 0; i < array.length; i++) {
            min = array[0][i];
            indexOfMin = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[0][j] < min) {
                    min = array[0][j];
                    indexOfMin = j;
                }
            }
            int temp = array[0][i];
            int temp1 = array[1][i];
            array[0][i] = array[0][indexOfMin];
            array[1][i] = array[1][indexOfMin];
            array[0][indexOfMin] = temp;
            array[1][indexOfMin] = temp1;
        }
    }

    //Метод по сдвигу элементов массива влево, крайнее левое значение затирается
    public static void arrayShift(int[][] arrOriginal, int arrTmp0, int arrTmp1, int count) {
        for (int i = 0; i < count; i++) {
            //Сдвигаем 1 элемент на 0, 2 на 1 если есть значение больше чем второй элемент
            arrOriginal[0][i] = arrOriginal[0][i + 1];
            arrOriginal[1][i] = arrOriginal[1][i + 1];
        }

        arrOriginal[0][count] = arrTmp0;
        arrOriginal[1][count] = arrTmp1;
    }

    public static String threeCharMaxCountToString2(char[] array) {
        //создаем массив на 123 элемента, дальше идут скобки и херь
        int[][] search = new int[2][124];

        //подсчет букв, в нулевой строке массива сохраняем код символа, в первой строке количество найденных букв
        for (int i = 0; i < array.length; i++) {
            search[0][array[i]] = array[i];
            search[1][array[i]] += 1;
        }

        //Для проверки количества символов. УДАЛИТЬ!!!
        int count = 0;
        for (int i = 0; i < search[0].length; i++) {
            System.out.print((char) search[0][i] + " - " + search[1][i] + ", ");
            count++;
            if (count == 20) {
                System.out.println();
                count = 0;
            }
        }
        System.out.println();

        //Двухмерный массив для выбора 3 наиболее встречающихся символов
        int[][] tmpArray = new int[2][3];

        int countTmpArray = 0; //Переменная для подсчета первоначального заполнения 3-х элементов в массиве
        // Цикл по перебору элементов и добавления их в массив 3-х элементов
        for (int i = 65; i < search[0].length; i++) {
            for (int j = tmpArray[0].length - 1; j >= 0; j--) {
                //Первоначальное заполнения
                if (search[1][i] > tmpArray[1][j] && tmpArray[1][j] == 0) {
                    tmpArray[0][j] = i;
                    tmpArray[1][j] = search[1][i];
                    countTmpArray++;
                    break;
                }
                //Дальнейшая проверка, в случае обнаружения смещаем все элементы влево до значения j
                if (search[1][i] > tmpArray[1][j] && tmpArray[1][j] != 0) {
                    arrayShift(tmpArray, search[0][i], search[1][i], j);
                    break;
                }
            }
            if (countTmpArray == 3) {
                intsCompare(tmpArray);
                System.out.println("Первоначальный массив заполнен");
                countTmpArray = -1; //Исправил на -1, чтобы больше не заходить в первоначальное заполнение массива
            }
        }

        System.out.println("До сортировки");
        for (int i = 0; i < 3; i++) {
            System.out.print((char) (tmpArray[0][i]) + " " + tmpArray[1][i] + "\n");
        }

        System.out.println("После сортировки");
        intsCompare(tmpArray);
        String str = "";
        for (int i = 0; i < 3; i++) {
            System.out.print((char) (tmpArray[0][i]) + " " + tmpArray[1][i] + "\n");
        }

        return str;
    }

    public static void main(String[] args) {
        //Scanner scanner = new Scanner(System.in);
        //char[] array = scanner.nextLine().toCharArray();
        String str = "erttyyghdvvcxbbbbbbbbEEEEEEbdfrrtkjhljoihftfaaaaadgaZZZZZZZZaaafjhgjmaaaannngfcccccfsssfeeeehhhhhhhhjhh"; //default array
        char[] array = str.toCharArray();

        String strOut = threeCharMaxCountToString2(array);
        System.out.println(strOut);
    }
}
