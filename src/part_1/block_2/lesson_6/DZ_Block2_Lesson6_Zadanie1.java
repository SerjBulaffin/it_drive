package part_1.block_2.lesson_6;


//1) Реализовать функцию сравнения строк (описано в уроке).
public class DZ_Block2_Lesson6_Zadanie1 {

    //Сравнение двух массивов char посимвольно
    //-1 если строка 'a' находится перед строкой 'b', 0 если равны, 1 если строка 'a' находится после строки 'b'
    public static int stringsCompare(char[] a, char[] b) {
        int len = 0;
        int result = 0;

        if (a.length > b.length)// проверка на минимальный размер длины массива, чтоб не выйти за пределы меньшего массива
            len = b.length;     // для итерации берем наименьший массив
        else
            len = a.length;

        for (int i = 0; i < len; i++) {
            if (a[i] < b[i]) {  // Если первый символ находиться левее второго, записываем -1 прерываем цикл, смысл
                result = -1;    // проверять его дальше
                break;
            } else if (a[i] == b[i]) {  // Если симвмолы равны, записываем в результат 0, продолжаем проверять следующий
                result = 0;             // символ
                continue;
            } else {        // Иначе первый символ находится правее второго, записываем в результат 1, прерываем цикл
                result = 1;
                break;
            }
        }

        // Если все-таки все символы наименьшего массива совпали с наибольшим
        // и мы получили результат 0, проверяем по длине массива
        if (result == 0 && a.length < b.length) {  // если массив А меньше массива В, значит он находится левее (-1).
            result = -1;
        } else if (result == 0 && a.length == b.length) { // Если длины совпали, то всте-таки 0.
            result = 0;
        } else if (result == 0 && a.length > b.length) {   // Если массив А больше Б по длине, тогда (1)
            result = 1;
        }

        return result;
    }

    public static void main(String[] args) {
        char[] a = {'a', 'e', 'c'};
        char[] b = {'a', 'e'};
        int result = stringsCompare(a, b);
        System.out.println(result);
    }
}
