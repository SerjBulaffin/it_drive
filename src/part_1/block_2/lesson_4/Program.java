package part_1.block_2.lesson_4;

public class Program {

    public static int a(int a1, int a2, int a3) {
        System.out.println("В методе а(), со значением на входе a1 = " + a1 + ", a2 = " + a2 + ", a3 = " + a3);
        int result = z(a1, a2) + z(a2, a3);
        System.out.println("В методе а(), значение на выходе = " + result);
        return result;
    }

    public static int z(int z1, int z2) {
        System.out.println("В методе z(), со значением на входе z1 = " + z1 + ", z2 = " + z2);
        int result = x(z1) * y(z2);
        System.out.println("В методе z(), значение на выходе = " + result);
        return result;
    }

    public static int y(int y1) {
        System.out.println("В методе y(), со значением на входе y1 = " + y1);
        int result = y1 + y1;
        System.out.println("В методе у(), значение на выходе = " + result);
        return result;
    }

    public static int x(int x1) {
        System.out.println("В методе х(), со значением на входе х1 = " + x1);
        int result = x1 * x1;
        System.out.println("В методе x(), значение на выходе = " + result);
        return result;
    }

    public static int f(int n) {
        System.out.println("В методе f(), со значение n на входе = " + n);
        if (n == 0) {
            System.out.println("В методе f(), результат на выходе = " + 1);
            return 1;
        }
        int result = f(n - 1) * n;
        System.out.println("В методе f(), результат на выходе = " + result);
        return result;
    }

    public static int fib(int n) {
        System.out.println("В методе fib(), значение на входе = " + n);
        if (n == 1 || n == 2) {
            System.out.println("В методе fib(), возвращаемый результат = " + 1);
            return 1;
        }
        int result = fib(n - 1) + fib(n - 2);
        System.out.println("В методе fib(), возвращаемый результат = " + result);
        return result;
    }

    public static void main(String[] args) {
        int result = fib(6);// 8
        System.out.println(result);
    }
}
