package part_1.block_2.lesson_4;

import java.util.Scanner;

//Реализовать функцию вычисления значения Фибоначчи с однократным вызовом рекурсии.
public class DZ_B2_Les4 {

    public static int fib(int n) {
        /*if (n == 1 || n == 2)
            return 1;*/
        return calcFib(1, 1, n);
    }

    public static int calcFib(int x1, int x2, int n) {
        int x3 = 0;
        if (n > 2) {
            x3 = x2 + x1;
            x1 = x2;
            x2 = x3;
            return calcFib(x1, x2, n-1);
        } else
            return x2;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите необходимый элемент числового ряда Фибонначи: ");
        int n = scanner.nextInt();
        int result = fib(n);
        System.out.println("Значение = " + result);
    }
}
