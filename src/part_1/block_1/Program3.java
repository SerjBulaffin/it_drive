package part_1.block_1;

public class Program3 {
    public static void main(String[] args) {
        int[] a = new int[5];
        a[0] = 3;
        a[1] = 4;
        a[2] = -1;
        a[3] = 5;
        a[4] = 9;

        int i = 0;
        while (i < 5) {
            System.out.print(a[i] + " ");
            i++;
        }
    }
}
