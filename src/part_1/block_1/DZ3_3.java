package part_1.block_1;

import java.util.Scanner;

public class DZ3_3 {

    /*public static long digitToBinaryString(int a) { //развернуть число
        long itog = 0;
        //int step = 10;

        while (a != 0) {
            int tmp = a % 2;
            if (tmp == 1) {
                itog = (itog * 10) + tmp;
            } else if (tmp == 1 && itog == 0) {
                itog = 1;
            } else if (tmp == 0 && itog == 0){
                itog = 1;
            } else {
                itog *= 10;
            }
            a /= 2;
        }

        return itog;
    }*/

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        int proverka = input;
        long itog = 0;
        long step = 0;

        while (input != 0) {
            if (input % 2 == 1 && step != 0) {
                itog += step;
            } else if (input % 2 == 1 && step == 0) {
                itog = 1;
                step = 1;
            } else if (input % 2 == 0 && step == 0) {
                step = 1;
            }
            input /= 2;
            step *= 10;
        }
        System.out.println(itog);
        System.out.println(Integer.toBinaryString(proverka));
        /*System.out.println(digitToBinaryString(input));
        System.out.println(Integer.toBinaryString(input));*/
    }
}


//3) Написать программу, которая выведет двоичное представление пятизначного числа.
/*
34765
1000011111001101
22347
101011101001011
99999
11000011010011111
 */
