package part_1.block_1;

import java.util.Scanner;

public class Program2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int proizved = 1;

        while (number != -1) {
            if (number % 2 == 0)
                proizved *= number;
            number = scanner.nextInt();
        }

        System.out.println("Proizvedenie ravno = " + proizved);
    }
}
