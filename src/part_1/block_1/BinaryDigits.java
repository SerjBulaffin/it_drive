package part_1.block_1;

import java.util.Scanner;

public class BinaryDigits {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int digit = scanner.nextInt();

        int num5 = (digit / 2) % 2;
        int num4 = (digit / 2) % 2;
        int num3 = (digit / 2) % 2;
        int num2 = (digit / 2) % 2;
        int num1 = (digit / 2) % 2;


        while (digit != 0) {
            int tmp = digit / 2;
            if (tmp * 2 == digit)
                System.out.print("0");
            else
                System.out.print("1");
            digit /= 2;
        }
    }
}
