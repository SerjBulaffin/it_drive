package part_1.block_1;

import java.util.Scanner;

public class Mirror5Digits {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Vvedite 5 - znach. chislo - ");
        int digits = scanner.nextInt();
        int reverse = 0;
        int el1 = digits % 10;
        digits /= 10;
        reverse += el1 * 10 * 10 * 10 * 10;

        int el2 = digits % 10;
        digits /= 10;
        reverse += el2 * 10 * 10 * 10;

        int el3 = digits % 10;
        digits /= 10;
        reverse += el3 * 10 * 10;

        int el4 = digits % 10;
        digits /= 10;
        reverse += el4 * 10;

        int el5 = digits % 10;
        digits /= 10;
        reverse += el5;

        System.out.println(reverse);
    }
}
