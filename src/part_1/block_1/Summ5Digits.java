package part_1.block_1;

import java.util.Scanner;

public class Summ5Digits {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите пятизначное число - ");
        int digit = scanner.nextInt();
        int summ = 0;
        for (int i = 0; i < 5; i++) {
            summ += digit % 10;
            digit /= 10;
        }
        System.out.println("Сумма пятизначного числа равна = " + summ);
    }
}
