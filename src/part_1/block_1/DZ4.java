package part_1.block_1;

import java.util.Scanner;

public class DZ4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        int proizved = 1;

        while (input != -1) {
            int tmp = input;
            int sum = 0;
            while (tmp != 0) {
                sum += tmp % 10;
                tmp /= 10;
            }
            if (sum % 2 == 0)
                proizved *= input;
            input = scanner.nextInt();
        }
        System.out.println("Proizvedenie ravno = " + proizved);
    }
}
/*
Реализовать программу, которая выводит произведение чисел бесконечной последовательности
(оканчивающейся на -1 ;), у которых сумма цифр - четное число.
123
45
19999
10
11
-1

Вот такая последовательность чисел, посмотрим на сумму цифр

1 + 2 + 3 =6 - четная сумма
4 + 5 = 9 - нечетная сумма
1 + 9 + 9 + 9 + 9 = 37 - нечетная сумма
1 + 0 = 1 - нечетная сумма
1 + 1 = 2 - четная сумма
-1

следовательно нас интересует произведение чисел 123 * 11

= 1353
 */
