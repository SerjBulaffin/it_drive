package part_1.block_1.lesson_7;

public class Main {

    public static int binarySearch(int[] array, int left, int right, int numberForSearch) {
        if (left == right) {
            return -1;
        }

        int middle = left + (right - left) / 2;

        if (array[middle] < numberForSearch) {
            return binarySearch(array, middle + 1, right, numberForSearch);
        } else if (array[middle] > numberForSearch) {
            return binarySearch(array, left, middle, numberForSearch);
        } else {
            return middle;
        }
    }

    public static void main(String[] args) {
        int array[] = {1, 2, 4, 6, 7, 8, 11, 55, 109, 205, 333, 405};
        int p1 = binarySearch(array, 0, array.length, 1);
        int p2 = binarySearch(array, 0, array.length, 405);
        int p3 = binarySearch(array, 0, array.length, 8);
        int p4 = binarySearch(array, 0, array.length, 100);

        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        System.out.println(p4);
    }
}
