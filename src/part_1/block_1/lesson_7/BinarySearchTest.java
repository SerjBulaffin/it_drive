package part_1.block_1.lesson_7;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BinarySearchTest {
    public static int array[] = {1, 2, 4, 6, 7, 8, 11, 55, 109, 205, 333, 405};

    @Test
    public void testSearchOn1() {
        int expected = 0;
        int actual = Main.binarySearch(array, 0, array.length, 1);
        assertEquals(expected, actual);
    }

    @Test
    public void testSearchOn8() {
        int expected = 5;
        int actual = Main.binarySearch(array, 0, array.length, 8);
        assertEquals(expected, actual);
    }

    @Test
    public void testSearchOn405() {
        int expected = 11;
        int actual = Main.binarySearch(array, 0, array.length, 405);
        assertEquals(expected, actual);
    }

    @Test
    public void testSearchOn100() {
        int expected = -1;
        int actual = Main.binarySearch(array, 0, array.length, 100);
        assertEquals(expected, actual);
    }
}
