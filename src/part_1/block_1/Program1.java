package part_1.block_1;

import java.util.Scanner;

public class Program1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int chet = 0;
        int noChet = 0;
        for(;;) {
            int number = scanner.nextInt();
            if (number == -1) {
                break;
            } else {
                if (number % 2 == 0) {
                    chet++;
                } else {
                    noChet++;
                }
            }
        }
        System.out.println("Четных чисел = " + chet);
        System.out.println("Нечетных чисел = " + noChet);
    }
}
