package part_1.block_1;

import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int sum = 0;
        for (int i = 0; i < 5; i++) {
            sum += a % 10;
            a /= 10;
        }
        System.out.println(sum);
    }
}
