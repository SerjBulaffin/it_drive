package part_1.block_1;

import java.util.Random;
import java.util.Scanner;

public class ProgramArraySearch {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[10];

        for (int i = 0; i < array.length; i++)
            array[i] = random.nextInt(100);

        for (int i = 0; i < array.length - 1; i++)
            System.out.print(array[i] + ", ");
        System.out.println(array[array.length - 1]);

        int min, indexOfMin;

        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;

            for (int j = i + 1; j < array.length; j++) {
                if (min > array[j]) {
                    min = array[j];
                    indexOfMin = j;
                }
            }
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }

        for (int i = 0; i < array.length - 1; i++)
            System.out.print(array[i] + ", ");
        System.out.println(array[array.length - 1]);

        boolean find = false;
        int numberForSearch = scanner.nextInt();
        int left = 0;
        int right = array.length - 1;
        int middle;

        while (left <= right) {
            middle = (right + left) / 2;
            if (array[middle] < numberForSearch) {
                left = middle + 1;
            } else if (array[middle] > numberForSearch) {
                right = middle - 1;
            } else {
                System.out.println("Exists.");
                find = true;
                break;
            }
        }

        if (find == false) {
            System.out.println("Not exists.");
        }
    }
}

