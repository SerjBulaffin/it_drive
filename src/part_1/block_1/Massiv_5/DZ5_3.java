package part_1.block_1.Massiv_5;

public class DZ5_3 {
    public static void main(String[] args) {
        int[] array = {58, 23, 57, 36, 98, 21, 12, 5, 66, 253, 1};

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();

        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

    }
}
//3) Выполнить разворот массива
// (первый элемент становится последним, второй - предпоследним и т.д.,
// последний элемент становится первым, предпоследний вторым и т.д.)
/*
заметки - Массив всегда делится пополам, четность или не четность количества элементов
не имеет значение, т.к. если нечетное количество элементов, то центральны элемент остается на месте
 */