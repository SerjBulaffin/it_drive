package part_1.block_1.Massiv_5;

public class DZ5_2 {
    public static void main(String[] args) {
        int[] array = {58, 23, 57, 36, 98, 21, 12, 5, 66, 253};
        double summ = 0;
        for (int i = 0; i < array.length; i++) {
            summ += array[i];
        }
        double sr = summ / array.length;
        System.out.println(sr);
    }
}

//2) Вывести среднее арифметическое элементов массива
