package part_1.block_1.Massiv_5;

public class DZ5_1 {
    public static void main(String[] args) {
        int[] array = {58, 23, -57, 36, 98, -21, 12, 5, 66, 253};
        int min = array[0];
        int max = array[0];
        int minIndex = 0;
        int maxIndex = 0;

        for (int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");
        System.out.println();

        for (int i = 1; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
                minIndex = i;
            }
            if (max < array[i]) {
                max = array[i];
                maxIndex = i;
            }
        }
        int temp = array[minIndex];
        array[minIndex] = array[maxIndex];
        array[maxIndex] = temp;
        //System.out.println(min + " " + max);

        for (int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");
    }
}

//1) Поменять местами максимальный и минимальный элемент массива.
