package part_1.block_1.Massiv_5;

import java.util.Scanner;

public class BinaryTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int inputD = scanner.nextInt();

        while (inputD != -1) {
            System.out.println(fiveDigitToBinary(inputD));
            System.out.println(Integer.toBinaryString(inputD));
            inputD = scanner.nextInt();
        }

    }

    public static long fiveDigitToBinary(int input) {
        long itog = 0;
        long step = 0;

        while (input != 0) {
            if (input % 2 == 1 && step != 0) {
                itog += step;
            } else if (input % 2 == 1 && step == 0) {
                itog = 1;
                step = 1;
            } else if (input % 2 == 0 && step == 0) {
                step = 1;
            }
            input /= 2;
            step *= 10;
        }
        return itog;
    }
}
