package part_1.block_1.Massiv_5;

public class Program {
    public static void main(String[] args) {
        int[] a = new int[5];
        a[0] = 5;
        a[1] = -1;
        a[2] = 3;
        a[3] = 2;
        a[4] = 6;

        int i = 0;
        while (i < 5) {
            System.out.print(a[i] + " ");
            i++;
        }

        System.out.println();

        i = 4;
        while (i >= 0) {
            System.out.print(a[i] + " ");
            i--;
        }
    }
}
