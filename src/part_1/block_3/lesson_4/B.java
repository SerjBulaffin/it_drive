package part_1.block_3.lesson_4;

public class B {
    private A a;

    void setA(A a) {
        this.a = a;
    }

    void method() {
        System.out.println("I'm from B!");
    }
}
