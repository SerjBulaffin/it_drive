package part_1.block_3.lesson_4;

public class A {
    private B b;

    void setB(B b) {
        this.b = b;
    }

    void method() {
        System.out.println("I'm from A!");
        b.method();
    }
}
