package part_1.block_3.lesson_4;

public class Main {
    public static void main(String[] args) {
        A a = new A();
        B b = new B();

        a.setB(b);
        b.setA(a);

        a.method();
    }
}
