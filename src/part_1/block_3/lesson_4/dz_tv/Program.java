package part_1.block_3.lesson_4.dz_tv;

public class Program {
    private char nameProgram[];

    Program(char nameProgram[]) {
        this.nameProgram = nameProgram;
    }

    char[] getNameProgram() {
        return this.nameProgram;
    }
}
