package part_1.block_3.lesson_4.dz_tv;

public class Main {
    public static void main(String[] args) {
        TV tv = new TV("SAMSUNG".toCharArray());
        RemoteController remoteController = new RemoteController();

        remoteController.setTV(tv);

        Channel channel1 = new Channel("CTC".toCharArray());
        Channel channel2 = new Channel("Ren TV".toCharArray());
        Channel channel3 = new Channel("Домашний".toCharArray());
        Channel channel4 = new Channel("TNT".toCharArray());
        Channel channel5 = new Channel("Illusion".toCharArray());

        tv.setChannel(channel1);
        tv.setChannel(channel2);
        tv.setChannel(channel3);
        tv.setChannel(channel4);
        tv.setChannel(channel5);

        Program program1 = new Program("ГОСТИ ИЗ ПРОШЛОГО".toCharArray());
        Program program2 = new Program("РАЗЛОМ САН-АНДРЕАС".toCharArray());
        Program program3 = new Program("ГОЛОДНЫЕ ИГРЫ: И ВСПЫХНЕТ ПЛАМЯ".toCharArray());
        channel1.setProgram(program1);
        channel1.setProgram(program2);
        channel1.setProgram(program3);

        Program program4 = new Program("Воздушная тюрьма".toCharArray());
        Program program5 = new Program("Водить по русски".toCharArray());
        Program program6 = new Program("Новости".toCharArray());
        channel2.setProgram(program4);
        channel2.setProgram(program5);
        channel2.setProgram(program6);

        Program program7 = new Program("Порча".toCharArray());
        Program program8 = new Program("Знахарка".toCharArray());
        Program program9 = new Program("Женский доктор".toCharArray());
        channel3.setProgram(program7);
        channel3.setProgram(program8);
        channel3.setProgram(program9);

        Program program10 = new Program("Где логика?".toCharArray());
        Program program11 = new Program("Дом 2 - Lite".toCharArray());
        Program program12 = new Program("Золото геленджика".toCharArray());
        channel4.setProgram(program10);
        channel4.setProgram(program11);
        channel4.setProgram(program12);

        Program program13 = new Program("Неудержимые 3".toCharArray());
        Program program14 = new Program("Шанхайские рыцари".toCharArray());
        Program program15 = new Program("Пианино".toCharArray());
        channel5.setProgram(program13);
        channel5.setProgram(program14);
        channel5.setProgram(program15);

        remoteController.on(3);

    }
}
