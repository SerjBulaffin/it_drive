package part_1.block_3.lesson_4.dz_tv;

import java.util.Random;

public class Channel {
    private Random random;
    private final int MAX_PROGRAM_COUNT = 3;
    private char nameChannel[];
    private Program program[];
    private int count;

    Channel(char nameChannel[]) {
        this.nameChannel = nameChannel;
        this.program = new Program[MAX_PROGRAM_COUNT];
        this.count = 0;
        this.random = new Random();
    }

    void setProgram(Program program) {
        if (count < MAX_PROGRAM_COUNT) {
            this.program[count] = program;
            this.count++;
        }
    }

    char[] getName() {
        return this.nameChannel;
    }

    //Рэндомный выбор просматриваемой передачи, на выбранном канале
    void selectProgram() {
        char nameProgram[];
        nameProgram = this.program[random.nextInt(count)].getNameProgram();
        System.out.print(" - Сейчас в эфире \"");
        for (int i = 0; i < nameProgram.length; i++) {
            System.out.print(nameProgram[i]);
        }
        System.out.print("\"");
    }
}
