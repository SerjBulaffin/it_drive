package part_1.block_3.lesson_4.dz_tv;

public class RemoteController {
    private TV tv;

    RemoteController() {

    }

    void setTV(TV tv) {
        this.tv = tv;
    }

    void on(int channelNumber) {
        tv.selectChannel(channelNumber - 1);
    }
}
