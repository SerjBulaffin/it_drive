package part_1.block_3.lesson_4.dz_tv;

public class TV {
    private final int MAX_CHANNEL_COUNT = 5;
    private char nameModel[];
    private Channel channel[];
    private int count;

    TV(char nameModel[]) {
        this.nameModel = nameModel;
        this.channel = new Channel[MAX_CHANNEL_COUNT];
        this.count = 0;
    }

    void setChannel(Channel channel) {
        if (this.count < MAX_CHANNEL_COUNT) {
            this.channel[count] = channel;
            this.count++;
        }
    }

    //Выбор просматриваемого канала, вызывается из RemoteController, передается номер канала
    void selectChannel(int numChannel) {
        Channel selectionChannel;
        char nameChannel[];

        if (numChannel > this.count || numChannel < 0) {
            selectionChannel = this.channel[0];
            nameChannel = selectionChannel.getName();
        } else {
            selectionChannel = this.channel[numChannel];
            nameChannel = selectionChannel.getName();
        }
        System.out.print("Канал " + (numChannel + 1) + " - \"");
        for (int i = 0; i < nameChannel.length; i++) {
            System.out.print(nameChannel[i]);
        }
        System.out.print("\"");

        //вызываем метод выбора программы в эфире, для текущего канала
        selectionChannel.selectProgram();
    }
}
