package part_1.block_3.lesson_4.primerBus;

public class Bus {
    private final int MAX_PASSENGER_COUNT = 5;

    private char number[];
    private Passenger passenger[];
    private Driver driver;
    private int count;

    Bus(char number[]) {
        this.number = number;
        passenger = new Passenger[MAX_PASSENGER_COUNT];
        this.count = 0;
    }

    void addPassenger(Passenger passenger) {
        if (count < MAX_PASSENGER_COUNT) {
            this.passenger[count] = passenger;
            passenger.setBus(this);
            count++;
        }
    }

    void setDriver(Driver driver) {
        this.driver = driver;
    }
}
