package part_1.block_3.lesson_4.primerBus;

public class Passenger {
    private char name[];
    private Bus bus;

    Passenger(char name[]) {
        this.name = name;
    }

    void setBus(Bus bus) {
        this.bus = bus;
    }
}
