package part_1.block_3.lesson_4.primerBus;

public class Main {
    public static void main(String[] args) {
        Passenger passenger1 = new Passenger("Passenger1".toCharArray());
        Passenger passenger2 = new Passenger("Passenger2".toCharArray());
        Passenger passenger3 = new Passenger("Passenger3".toCharArray());
        Driver driver = new Driver("Степан".toCharArray());

        Bus bus = new Bus("p535py22".toCharArray());
        bus.addPassenger(passenger1);
        bus.addPassenger(passenger2);
        bus.addPassenger(passenger3);

        bus.setDriver(driver);
        driver.setBus(bus);
    }
}
