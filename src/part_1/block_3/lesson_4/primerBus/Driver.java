package part_1.block_3.lesson_4.primerBus;

public class Driver {
    private char name[];
    private Bus bus;

    Driver(char name[]) {
        this.name = name;
    }

    void setBus(Bus bus) {
        this.bus = bus;
    }
}
