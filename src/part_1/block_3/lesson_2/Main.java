package part_1.block_3.lesson_2;

public class Main {
    public static void main(String[] args) {
        Human h1 = new Human(); // h1 указывает на реальный объект в памяти, котрый имеет тип Human
        h1.height = 1.5;
        h1.grow(1.5);

        Human humans[] = new Human[5]; //Массив из пяти указателей на реальные объекты людей
        humans[0] = new Human();
        humans[1] = new Human();
    }
}
