package part_1.block_3.lesson_2;

public class Human {
    double height;
    boolean isWorker;

    void grow (double value) {
        height += value;
    }

    void work() {
        isWorker = true;
    }

    void relax() {
        isWorker = false;
    }
}
