package part_1.block_3.lesson_3;

public class Main {
    public static void main(String[] args) {
        //Конструктор создает начальное состояние объекта
        //т.е. конкретные значения его полей
        Rectangle r = new Rectangle(5, 10); //Использование конструктора с параметрами
        Rectangle r1 = new Rectangle(); //Конструктор без параметров (пустой)
        Rectangle r2 = new Rectangle(r1); //Использование конструктора копирования

        r.setHeight(15);
        r.setLength(20);

        System.out.println(r.getLength());

        double x = r.calcArea();
        System.out.println(x);
    }
}
