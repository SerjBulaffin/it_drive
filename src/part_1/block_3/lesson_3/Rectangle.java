package part_1.block_3.lesson_3;

public class Rectangle {
    //поля, fields
    private double length;
    private double height;

    //Пустой конструктор
    Rectangle() {
        this.length = 1;
        this.height = 1;
    }

        //Конструктор с параметрами
    Rectangle(double length, double height) { //Коструктор (в скобках формальные параметры)
        setLength(length);
        setHeight(height);
    }

    //Конструктор копирования
    Rectangle(Rectangle original) {
        this.length = original.length;
        this.height = original.height;
    }

    //setter - кладем значения в поле объекта
    void setLength(double length) {
        if (length >= 0) {
            this.length = length;
        } else {
            this.length = 0;
        }
    }

    void setHeight(double height) {
        if (height >= 0) {
            this.height = height;
        } else {
            this.height = 0;
        }
    }

    //getter - возвращает значение поля объекта
    double getLength() {
        return this.length;
    }

    double getHeight() {
        return this.height;
    }

    //метод вычисления площади прямоугольника
    double calcArea() {
        return this.length * this.height;
    }
}
