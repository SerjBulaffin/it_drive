package part_1.block_3.lesson_1;

import java.util.Scanner;

public class Program2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a[][];
        System.out.println("Введите количество строк в массиве:");
        int n = scanner.nextInt();
        a = new int[n][];
        for (int i = 0; i < a.length; i++) {
            System.out.println("Введите количество элементов в строке " + i + ":");
            int currentRowElementsCount = scanner.nextInt();
            System.out.println("Введите " + currentRowElementsCount + " элементов строки: ");
            a[i] = new int [currentRowElementsCount];//задаем количество элементов в строке i
            for (int j = 0; j < a[i].length; j++) {
                a[i][j] = scanner.nextInt();
            }
        }

        //вывод массива
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
