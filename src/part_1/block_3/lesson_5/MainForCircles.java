package part_1.block_3.lesson_5;

public class MainForCircles {
    public static void main(String[] args) {
        Circle c1 = new Circle(10);
        Circle c2 = new Circle(15);
        Circle c3 = new Circle(20);
        Circle c4 = new Circle(25);
        Circle c5 = new Circle(30);

        Circle circles[] = {c1, c2, c3, c4, c5};
        System.out.println(Circle.getAreaForCircles(circles));
    }
}
