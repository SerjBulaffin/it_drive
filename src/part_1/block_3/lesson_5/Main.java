package part_1.block_3.lesson_5;

public class Main {
    public static void main(String[] args) {
        System.out.println(SomeClass.someStaticField);
        SomeClass a = new SomeClass();
        SomeClass b = new SomeClass();
        a.someField = 10;
        b.someField = 20;

        a.someStaticField = 100;
        a.someStaticField = 200;

        SomeClass.someStaticField = 300;

        System.out.println(a.someField);
        System.out.println(b.someField);
        System.out.println(a.someStaticField);
        System.out.println(b.someStaticField);

        a.someMethod();
        SomeClass.someStaticMethod();
    }
}
