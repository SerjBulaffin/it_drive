package part_1.block_3.lesson_5;

public class Circle {
    private double radius;

    public Circle(double radius) {
        if (radius >= 0) {
            this.radius = radius;
        } else {
            this.radius = 0;
        }
    }

    double getArea() { //Вычисление площади круга
        return Math.PI * (radius * radius);
    }

    static double getAreaForCircles(Circle circles[]) { //Вычисление площади всех объектов
        double result = 0;
        for (int i = 0; i < circles.length; i++) {
            result += circles[i].getArea();
        }
        return result;
    }
}
