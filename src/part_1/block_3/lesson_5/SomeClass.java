package part_1.block_3.lesson_5;

import java.util.Random;

public class SomeClass {
    int someField; // данное поле для каждого объекта свое
    static int someStaticField = 500; // статическое поле является глобальным для всех
                                // объектов SomeClass и имеет отношение только к классу

    static { // Инициализируется после инициализации полей класса
        Random random = new Random();
        someStaticField = random.nextInt();
    }

    void someMethod() {
        System.out.println(someField);
        System.out.println(someStaticField);
    }

    static void someStaticMethod() {
        System.out.println(someStaticField);
        //System.out.println(someField); //Внутри статического метода мы не имеем доступ к нестатическим полям.
    }
}
