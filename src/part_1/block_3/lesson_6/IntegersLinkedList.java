package part_1.block_3.lesson_6;

public class IntegersLinkedList {
    private Node first; //первый элемент списка
    private Node last; //последний элемент списка
    private int count; //количество элементов в списке

    public IntegersLinkedList() { //В конструкторе обнуляем count
        this.count = 0;
    }

    public void add(int element) {
        Node newNode = new Node(element); //Создаем ноду со значением element
        if (first == null) { //Если первый элемент равен null, тогда первому элементу
            first = newNode; //присваиваем значение newNode
            last = newNode; //так же и последнему элементу присваиваем newNode,
        } else {
            /*Node current = first;
            while (current.getNext() != null) {
                current = current.getNext();
            }
            current.setNext(newNode);*/
            this.last.setNext(newNode);
            this.last = newNode;
        }
        this.count++;
    }

    public void add(int element, int index) { //добаление элемента в позицию index
        if (index < 0 || index > count) { //если индекс меньше 0 или больше count, возвращаемся в вызываемый метод
            return;
        }
        Node newNode = new Node(element); //Создаем ноду со значением element
        Node current = first; //создаем Ноду со ссылкой на первый элемент
        for (int i = 0; i < index - 1; i++) { //передвигаемся до позиции вставки
            current = current.getNext();
        }
        Node nextNode = current.getNext(); //создаем Ноду на следующий элемент от current
        newNode.setNext(nextNode); //привязываем к новой ноде все что идет дальше позиции вставки
        current.setNext(newNode); //привязываем к ноде предшествующей вставке newNode
        this.count++; //увеличиваем кол-во нод в листе
    }

    //TODO Не работатет, элемент не удаляет, уменьшает count
    void remove(int index) { //удаление элемента из листа
        if (index < 0 || index > count) { //если индекс меньше 0 или больше count, возвращаемся в вызываемый метод
            return;
        }
        Node current = first; //создаем Ноду со ссылкой на первый элемент
        for (int i = 0; i < index - 1; i++) { //передвигаемся до позиции удаления
            current = current.getNext();
        }

        Node nextNode = current.getNext(); //создаем Ноду на следующий элемент от current
        nextNode.getNext();
        nextNode.getNext();
        current.setNext(nextNode);
        this.count--;
    }

    public int get(int index) {
        Node current = first; //Создаем текущую Ноду на первый элемент
        int result = 0;

        if (index < 0 || index > count) { //если индекс меньше 0 или больше count, возвращаем -1
            result = -1;
        } else {
            for (int i = 0; i < count; i++) { //перебираем все элементы по очереди
                if (i == index) { //при равенстве i и index
                    result = current.getValue(); //записываем в результат значение value текущей ноды
                    break; //прерываем цикл
                }
                current = current.getNext(); //если не равно, присваиваес current следующий элемент
            }
        }

        return result; //возвращаем результат
    }

    public void printList() {
        Node current = first;
        for (int i = 0; i < count; i++) {
            System.out.print(current.getValue() + " ");
            current = current.getNext();
        }
    }
}
