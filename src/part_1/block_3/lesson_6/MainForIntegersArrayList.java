package part_1.block_3.lesson_6;

public class MainForIntegersArrayList {
    public static void main(String[] args) {
        IntegersArrayList list = new IntegersArrayList();

        list.add(7);
        list.add(12);
        list.add(16);
        list.add(20);
        list.add(25);
        list.add(30);
        list.add(35);
        list.add(40);
        list.add(45);
        list.add(50);

        list.add(255, 0);

        list.add(300);
        list.add(400);

        list.printArray();

        list.reverse();

        list.printArray();
        list.remove(2);
        list.printArray();
    }
}
