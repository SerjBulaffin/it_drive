package part_1.block_3.lesson_6;

public class MainForIntegersLinkedList {
    public static void main(String[] args) {
        IntegersLinkedList linkedList = new IntegersLinkedList();
        linkedList.add(9);
        linkedList.add(7);
        linkedList.add(-3);
        linkedList.add(6);
        linkedList.add(5);

        linkedList.printList();
        System.out.println();

        linkedList.add(25, 2);
        linkedList.printList();
        System.out.println();
        linkedList.remove(2);
        linkedList.printList();
    }
}
