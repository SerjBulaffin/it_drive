package part_1.block_3.lesson_6;

public class IntegersArrayList {
    private static final int DEFAULT_ARRAY_SIZE = 10;

    private int elements[]; //массив элементов int
    private int count;

    public IntegersArrayList() { //Конструктор
        this.elements = new int[DEFAULT_ARRAY_SIZE]; //Инициализируем значением по умолчанию
        this.count = 0; //текущее количество элементов 0
    }

    public void add(int element) { //Добавление элементов
        if (count < this.elements.length) { //если количество меньше длины массива
            this.elements[count] = element; //добавляем элемент в позицию count
            count++; //увеличиваем count на 1
        } else {
            System.err.println("Array out of bounds"); //иначе выкидываем ошибку
        }
    }

    public void add(int element, int index) { //вставка элемента в массив на позицию index
        if (index < 0 && index > elements.length) {
            return;
        }

        if (count + 1 <= this.elements.length) { //если количество меньше длины массива
            this.elements[count] = element; //добавляем элемент в конец массива
            count++; //увеличиваем количество элементов в массиве на один
            for (int i = count - 1; i > index; i--) { //смещаем последний элемент с предыдущим, пока не дойдем до index
                int tmp = this.elements[i];
                this.elements[i] = this.elements[i - 1];
                this.elements[i-1] = tmp;
            }
        } else { //если при добавленн count выходит за длину массива
            int tempArray[] = new int[(int) (this.elements.length * 1.5)]; //создаем временный массив в 1,5 раза больше elements
            for (int i = 0; i < this.elements.length; i++) { //копируем все элементы из elements в tempArray
                tempArray[i] = this.elements[i];
            }
            tempArray[count] = element; //добавляем элемент в конец массива
            count++; //увеличиваем количество элементов в массиве на один
            for (int i = count - 1; i > index; i--) {
                int tmp = tempArray[i];
                tempArray[i] = tempArray[i - 1];
                tempArray[i-1] = tmp;
            }
            elements = tempArray; //
        }
    }

    void remove(int index) { //удаление элемента из массива, в позиции index
        if (index < 0 && index > count) {
            return;
        }
        elements[index] = 0;
        for (int i = index; i < count; i++) {
            int temp = elements[i];
            elements[i] = elements[i+1];
            elements[i + 1] = temp;
        }
        count--;
    }

    void reverse() { //разворот массива
        for (int i = 0; i < count / 2; i++) { //за длину берем фактическое количество элементов в массиве
            int temp = elements[i];
            elements[i] = elements[count - 1 - i];
            elements[count - 1 - i] = temp;
        }
    }

    public int size() { //размерность массива
        return elements.length;
    }

    public int getCount() { //количество элементов в массиве
        return count;
    }

    public int get(int index) { //возврат элемента из массива
        if (index >= 0 && index < count) { //если индекс больше или равно 0 и меньше count
            return this.elements[index]; //возвращаем элемент в позиции index
        } else {
            System.err.println("Index out of range"); //иначе выкидываем ошибку
            return -1;
        }
    }

    public void printArray() {
        for (int i = 0; i < count; i++) {
            System.out.print(elements[i] + " ");
        }
        System.out.println("\nРазмерность массива " + elements.length + ", количество элементов " + count);
    }

}
