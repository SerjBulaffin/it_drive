package part_1.weekly_1.function;

public class Program {

    public static double average(int[] a) {
        double average = 0;
        for (int i = 0; i < a.length; i++) {
            average += a[i];
        }
        System.out.println("Сумма равна = " + average + ", количество элементов = " + a.length);
        return average / a.length;
    }

    public static int findMin(int[] a) {
        int min = a[0];
        for (int i = 1; i < a.length; i++) {
            if (min > a[i])
                min = a[i];
        }
        return min;
    }

    public static void main(String[] args) {
        int[] array = {0, -5, 4, 7, 9, 12, -48, 125, 19};
        System.out.println(findMin(array));
        System.out.println(average(array));
    }
}
