package part_1.weekly_1.function;

import java.util.Scanner;

public class DZ1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int znak = scanner.nextInt();
        int znach = scanner.nextInt();
        System.out.println(summ(znak, znach));
        System.out.println(summ1(znach));

    }

    public static int summ(int znak, int znach) {
        int summa = 0;
        for (int i = 0; i < znak; i++) {
            summa += znach % 10;
            znach /= 10;
        }
        return summa;
    }

    public static int summ1(int a) {
        int summa = 0;
        if (a == 0)
            return summa;
        summa += a % 10;
        a /= 10;
        summ1(a);

        return summa;
    }
}
